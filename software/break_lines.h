#ifndef _BREAK_LINES_H_INCLUDED_
#define _BREAK_LINES_H_INCLUDED_

#include <string.h>

#define MAXBREAKLINES 255

char **break_lines( char *s, int *n);

#endif
