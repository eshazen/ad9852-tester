#!/bin/bash

# set up frequency shift keying

FREQ1="200e3"
FREQ2="100e3"

PROG="./dds_conf.exe"

# reset the chip
$PROG -r

# set the update clock to 16
$PROG -c write 0x19 0x10

# setup the DDS for FSK
$PROG -c write 0x1f 3

# set frequencies
$PROG -f1 $FREQ1

$PROG -f2 $FREQ2

# disable OSK_EN for full-amplitude output
$PROG -c write 0x20 00

$PROG -c fsk
