
#include "break_lines.h"

//
// break return string into lines
// return a (static) list of pointers to the lines
//  
char **break_lines( char *s, int *n)
{
  static char* lines[MAXBREAKLINES];
  char *p;
  char **r;
  int nlines = 0;
  
  p = strtok( s, "\r\n");
  lines[nlines++] = p;
  while( (p = strtok( NULL, "\r\n"))) {
    lines[nlines++] = p;
    if( nlines >= MAXBREAKLINES)
      return NULL;
  }

  *n = nlines;
  
  return lines;
}

