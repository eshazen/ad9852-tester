//
// Test AD9852 using microcontroller thing
//
//
// usage:
//
// Serial commands used:
// "write <addr> <data>"
// "read <addr>"
//

#include "sio_cmd.h"
#include "calc_freq.h"
#include "break_lines.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <inttypes.h>

// number of regs on AD chip
#define DDS_REGS 0x28


//
// main program
//

int main( int argc, char *argv[]) {

  char buff[255];
  char cmd[255];
  char *s;
  int fd;
  int n;
  int i;
  char **lines;
  uint8_t regs[DDS_REGS];

  char opt;
  
  if( argc < 2) {
    printf("usage: check_regs [-t]        # test all regs\n");
    printf("                  [-d]        # read and dump regs\n");
    printf("                  [-r]        # reset the chip\n");
    printf("                  [-f1 freq]  # set frequency 1\n");
    printf("                  [-f2 freq]  # set frequency 2\n");
    printf("                  [-c cmd]    # send command\n");
    exit( 1);
  }

  opt = toupper( argv[1][1]);
  if( argv[1][0] != '-' || !isalpha( argv[1][1]) || !index( "TDRCF", opt)) {
    printf("Unknown option: %s\n", argv[1]);
  }

  if( (fd = sio_open("/dev/ttyUSB0", 10)) < 0) {
    printf("Error opening serial port\n");
    exit( 1);
  }

  if( opt == 'F') {
    if( argc < 3) {
      printf("Missing frequency after -f\n");
      exit(1);
    }

    uint64_t iftw = calc_freq( atof( argv[2]));
    printf( "IFTW = 0x%012" PRIx64 " (%" PRIu64 ")\n", iftw, iftw);

    uint8_t rega;		/* FTW address */

    switch( argv[1][2]) {
    case '1':
      rega = 9;
      break;
    case '2':
      rega = 0xf;
      break;
    default:
      printf("Must be '1' or '2' after -f (saw %c)\n", argv[1][2]);
      exit(1);
    }

    // loop over and send 6 bytes to rega
    for( i=0; i<6; i++) {
      uint8_t b = (iftw >> (8*i)) & 0xff;
      sprintf( cmd, "write %d 0x%02x", rega-i, b);
      printf("%s\n", cmd);
      s = sio_cmd( fd, cmd, 0);
    }

  }

  // build the command line
  if( opt == 'C') {
    if( argc < 3) {
      printf("Missing command after -c\n");
      exit(1);
    }
    *cmd = '\0';
    for( i=2; i<argc; i++) {
      strcat( cmd, argv[i]);
      if( i < argc-1)
	strcat( cmd, " ");
    }
    printf("Command: \"%s\"\n", cmd);
    s = sio_cmd( fd, cmd, 0);
    lines = break_lines( s, &n);
    if( n > 2)
      printf("result: %s\n", lines[1]);
  }

  // reset
  if( opt == 'R') {
    printf("Reset the chip\n");
    s = sio_cmd( fd, "reset", 0);
  }


  // write all regs with register no in low 5 bits,
  // upper bits all 1's and try to read

  if( opt == 'T') {
    printf("Writing... ");

    for( i=0; i<DDS_REGS; i++) {
      printf("%d ", i);
      fflush( stdout);
      regs[i] = 0xe0 | (i & 0x1f);
      sprintf( cmd, "write %d 0x%02x", i, regs[i]);
#ifdef DEBUG
      printf("%s\n", cmd);
#endif    
      s = sio_cmd( fd, cmd, 0);
      lines = break_lines( s, &n);
    }
  }
  
  if( opt == 'T' || opt == 'D') {
    printf("\nReading...\n");


    printf("addr write  = read  diff\n");
    printf("---- -----    ----  ----\n");


    for( i=0; i<DDS_REGS; i++) {
      sprintf( cmd, "read %d", i);
      s = sio_cmd( fd, cmd, 0);
      lines = break_lines( s, &n);
      unsigned long v = strtoul( lines[1], NULL, 16);
      uint8_t d = regs[i] - (v & 0xff);
      if( d && (opt != 'D'))
	printf( "0x%02x (0x%02x) = 0x%02x  0x%02x\n", i, regs[i], (int)v, d);
      else
	printf( "0x%02x (0x%02x) = 0x%02x\n", i, regs[i], (int)v);
    }
  }
    

}
