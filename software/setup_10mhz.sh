#!/bin/bash

PROG="./dds_conf.exe"

# setup the DDS for single tone

# reset the chip
$PROG -r

# set the update clock to 16
$PROG -c write 0x19 0x10

# set the frequency word 1 for 2MHz
$PROG -f1 2e6

# disable OSK_EN for full-amplitude output
$PROG -c write 0x20 00
