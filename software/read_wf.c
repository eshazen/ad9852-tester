//
// read ascii waveform from specified channel
//
// usage: read_wf <ch> <start> <stop>
//

#include "sio_cmd.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define DEBUG

#define MAXLINES 255

//
// break return string into lines
// return a (static) list of pointers to the lines
//  
char **break_lines( char *s, int *n)
{
  static char* lines[MAXLINES];
  char *p;
  char **r;
  int nlines = 0;
  
  p = strtok( s, "\r\n");
  lines[nlines++] = p;
  
  while( (p = strtok( NULL, "\r\n"))) {
    lines[nlines++] = p;
  }

  *n = nlines;
  
  return lines;
}


int main( int argc, char *argv[]) {

  char buff[255];
  char *s;
  int fd;
  int n;
  int i;
  char **lines;

  if( (fd = sio_open("/dev/ttyUSB0", 10)) < 0) {
    printf("Error opening serial port\n");
    exit( 1);
  }

  s = sio_cmd( fd, "read 0", 0);

  lines = break_lines( s, &n);
  printf("%d lines read\n", n);
  for( i=0; i<n; i++)
    printf("[%d] = \"%s\"\n", i, lines[i]);


}
