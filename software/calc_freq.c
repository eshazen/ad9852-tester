//
// calculate frequency tuning word for AD9852
// from:
//   FTW = (Desired Output Frequency * 2^N) / SYSCLK
// where:
//   2^N = 2^48
//   SYSCLK = oscillator * PLL mult (for us, 49.152MHz * 4)
//

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

// frequency word calculator
#define REFCLK (49.152e6)
#define PLLMUL 1
#define SYSCLK (REFCLK*PLLMUL)

uint64_t calc_freq( double freq) {

  double dftw, dpow;
  uint64_t iftw, ifreq;
  int i;
  char res[50];

  ifreq = freq;

  dpow = (double)(1LL << 48);

  dftw = (freq * dpow) / (double)SYSCLK;
  iftw = dftw;

  return iftw;
}
