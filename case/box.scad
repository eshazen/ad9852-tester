
//
// Box to hold an Apollo CM test mezzanine as a stand-alone thing
//

// dims in inches
e = 0.01;
$fn = 32;

// board outline
module board_outline() {
  polygon( points = [ [0, 0.5], [0, 3.5], [1, 3.5], [1, 4],
		      [5, 4], [5, 0], [1, 0], [1, 0.5]]);
}

// mounting hole locations
holes = [ [1.18, 0.200], [1.18, 3.8]];
hole_dia = 0.1;

board_thk = 0.063;


module hole_at( x, y) {
  translate( [x, y, -e])
    cylinder( h = board_thk+2*e, r=hole_dia/2);
}

module board() {
  difference() {
    linear_extrude( height=board_thk) { board_outline(); }
    for( h = holes) { hole_at( h[0], h[1]); }
  }
}





board();
