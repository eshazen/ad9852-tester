# ad9852-tester

This is a tester/programmer for the Analog Device AD9852 evaluation
board and compatible boards.  Essentially it consists of a board
recycled from another EDF project ("APollo Command Module Tester"):

https://gitlab.com/BU-EDF/edf-projects/apollocm/templatecm/

This board has an ATMega325PV microcontroller with most of the pins
routed to a ribbon cable connector, which conveniently has the same
pinout as the control connector on the AD eval board.  Also included
is a USB/serial adapter for communication with a computer.

The directory "breakout" has the PCB design for a breakout board
to interface with an NI PXIe-7858 module via a 68 pin VHDCI connector.

The directory "firmware" has a program for the ATMega to accept
simple serial commands to control the DDS.

The directory "software" has a C program which communicates with
the uC for testing.

Firmware and software build under Ubuntu Linux 20.04 with the
avr-gcc and avr-libc packages installed.
