#! /bin/bash

avr-gcc -mmcu=atmega325p -Wall -Os -o blink.elf blink.c
avr-objcopy -j .text -j .data -O ihex blink.elf blink.hex
avrdude -p m325p -c avrisp2 -e -U flash:w:blink.hex