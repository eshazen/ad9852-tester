// Normal Blinking Code:

// #ifndef F_CPU
// #define F_CPU 16000000UL // 16 MHz clock speed
// #endif

// #include <avr/io.h>
// #include <util/delay.h>

// int main(void)
// {
//   DDRE = 0xFF; //Makes PORTE as Output
//   while(1) //infinite loop
//   {
//     PORTE = 0xF0; //Turns ON All LEDs
//     _delay_ms(50); //0.5 second delay
//     PORTE = 0x00; //Turns OFF All LEDs
//     _delay_ms(50); //0.5 second delay
//   }
// }


// Counter:

#ifndef F_CPU
#define F_CPU 16000000UL // 16 MHz clock speed
#endif

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
  DDRE = 0xFF; //Makes PORTE as Output
  int count = 0b00000001;
  while(1) //infinite loop
  {
    PORTE = count << 4; // assigns values to the LEDs
    _delay_ms(100); //1 second delay
    count += 0b00000001; // increment by 1
  }
}