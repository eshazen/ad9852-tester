#include "SPI_cmds.h"


//------------------------------------------------------------------------------
void SPI_setup(void)
{
  DDRB |= (1<<2)|(1<<3)|(1<<5);    // CS, MOSI and SCK as outputs
  DDRB &= ~(1<<4);                 // MISO as input

  SPCR |= (1<<MSTR);               // Set as Master
  SPCR &= ~((1<<SPR0)|(1<<SPR1));  // set SCK(system clock) as F(osc)/2
  SPSR |= (1<<SPI2X);              // set SCK as F(osc)/2 
  
  SPCR |= (1<<SPE);                // Enable SPI

}

//------------------------------------------------------------------------------
uint8_t SPI_transfer_blocking(uint8_t send_data)
{
  SPDR = send_data;            // send the data
  while(!(SPSR & (1<<SPIF)));  // wait until transmission is complete
  uint8_t read_data = SPDR;
  return read_data;
}

//------------------------------------------------------------------------------
void SPI_transfer_non_blocking(uint8_t send_data)
{
  SPDR = send_data;            // send the data
}

//------------------------------------------------------------------------------
uint8_t statusreg(void){
  PORTB &= ~(1<<2); //set chip select pin low
  SPI_transfer_blocking(0x05);
  uint8_t data1 = SPI_transfer_blocking(0x00);
  PORTB |= (1<<2); //set chip select pin high
  
  return data1;

}

//------------------------------------------------------------------------------
void check_WIP(void){
  while (statusreg()&0x01);
}
