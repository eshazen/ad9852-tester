#ifndef __DDS_H__
#define __DDS_H__

#include <stdint.h>
#include <avr/io.h>

void dds_setup();
void dds_data_out();
void dds_data_in();
void dds_leds( uint8_t n);
void dds_addr( uint8_t a);
void dds_data( uint8_t d);
void dds_reset();
uint8_t get_dds_data();

void dds_write( uint8_t a, uint8_t d);
uint8_t dds_read( uint8_t a);

//
// FSK output on extra connector pin 9
//
#define DDS_FSK       _BV(PA6)
#define DDS_FSK_PORT  PORTA
#define DDS_FSK_PIN   PINA


//
// DDS pins
//

#define DDS_LED0     _BV(PE4)
#define DDS_LED1     _BV(PE5)
#define DDS_LED2     _BV(PE6)
#define DDS_LED3     _BV(PE7)

#define DDS_MRESET   _BV(PD0)
#define DDS_OSK      _BV(PD1)
#define DDS_PMODE    _BV(PD2)
#define DDS_RDB      _BV(PD3)
#define DDS_WRB      _BV(PD4)
#define DDS_IO_UD    _BV(PD5)
#define DDS_A0       _BV(PD6)
#define DDS_A1       _BV(PD7)

#define DDS_A2       _BV(PC0)
#define DDS_A3       _BV(PC1)
#define DDS_A4       _BV(PC2)
#define DDS_A5       _BV(PC3)

#define DDS_D0       _BV(PC4)
#define DDS_D1       _BV(PC5)
#define DDS_D2       _BV(PC6)
#define DDS_D3       _BV(PC7)

#define DDS_D4       _BV(PF7)
#define DDS_D5       _BV(PF6)
#define DDS_D6       _BV(PF5)
#define DDS_D7       _BV(PF4)

//
// DDS ports
//
#define DDS_LED0_PORT PORTE
#define DDS_LED1_PORT PORTE
#define DDS_LED2_PORT PORTE
#define DDS_LED3_PORT PORTE

#define DDS_MRESET_PORT   PORTD
#define DDS_OSK_PORT      PORTD
#define DDS_PMODE_PORT    PORTD
#define DDS_RDB_PORT      PORTD
#define DDS_WRB_PORT      PORTD
#define DDS_IO_UD_PORT    PORTD

#define DDS_A0_PORT       PORTD
#define DDS_A1_PORT       PORTD
#define DDS_A2_PORT       PORTC
#define DDS_A3_PORT       PORTC
#define DDS_A4_PORT       PORTC
#define DDS_A5_PORT       PORTC

#define DDS_D0_PORT       PORTC
#define DDS_D1_PORT       PORTC
#define DDS_D2_PORT       PORTC
#define DDS_D3_PORT       PORTC
#define DDS_D4_PORT       PORTF
#define DDS_D5_PORT       PORTF
#define DDS_D6_PORT       PORTF
#define DDS_D7_PORT       PORTF

// input ports
#define DDS_MRESET_PIN   PIND
#define DDS_OSK_PIN      PIND
#define DDS_PMODE_PIN    PIND
#define DDS_RDB_PIN      PIND
#define DDS_WRB_PIN      PIND
#define DDS_IO_UD_PIN    PIND

#define DDS_A0_PIN       PIND
#define DDS_A1_PIN       PIND
#define DDS_A2_PIN       PINC
#define DDS_A3_PIN       PINC
#define DDS_A4_PIN       PINC
#define DDS_A5_PIN       PINC

#define DDS_D0_PIN       PINC
#define DDS_D1_PIN       PINC
#define DDS_D2_PIN       PINC
#define DDS_D3_PIN       PINC
#define DDS_D4_PIN       PINF
#define DDS_D5_PIN       PINF
#define DDS_D6_PIN       PINF
#define DDS_D7_PIN       PINF

#define DDS_LED0_PIN PINE
#define DDS_LED1_PIN PINE
#define DDS_LED2_PIN PINE
#define DDS_LED3_PIN PINE

#endif

