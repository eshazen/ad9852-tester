#include <stdlib.h>
#include <ctype.h>
#include <util/delay.h>

#include "utils.h"
#include "cmds.h"
#include "SPI_cmds.h"
#include "UART_funcs.h"
#include "NeoPixel.h"
#include "dds.h"

#define TDC_FW_2X 0x171240 //two times the size of the hex file

#define FLASH_BUFF_SIZE 128 //number of bytes to be writen to and read from the flash per read/write 
#define FLASH_TO_FPGA_SIZE 1

volatile uint8_t fpga_status = 0; //initially the fpgas are unprogrammed

const char* version_num = "1.00";

uint8_t flashbuff[FLASH_BUFF_SIZE];
uint32_t flash_address = 0;

uint8_t program_interrupt = 0;

//------------------------------------------------------------------------------
void cmd_echo(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //Serial.println("ECHO:");
  for (uint8_t position = 0;(position<MAX_ARGS)&&(arg_size[position] !=0); position++){
    UART_print_noline(args[position]);
    // if (args[position][1] != 0x00 || 0x20){
    UART_print_noline(" ");
    //}
  }
  UART_newline();        
}


//------------------------------------------------------------------------------
void cmd_led(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  uint8_t led_value = strtoul( args[0], NULL, 0);
  dds_leds( led_value);
  UART_print_hex( led_value);
  UART_newline();
}

//------------------------------------------------------------------------------
void cmd_version(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  UART_print_noline("DDS Tester ");
  UART_print_noline(version_num);
}

//------------------------------------------------------------------------------
void cmd_write(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  uint8_t a = strtoul( args[0], NULL, 0);
  uint8_t d = strtoul( args[1], NULL, 0);
  dds_write( a, d);
}

//------------------------------------------------------------------------------
void cmd_read(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  uint8_t a = strtoul( args[0], NULL, 0);
  uint8_t d;

  d = dds_read( a);

  UART_print_hex( d);
  UART_newline();
}

//
// temporarily set all ports as output and walk a '1' thru them
//
void cmd_walk(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  uint8_t d = 1;

  UART_print("walking 1s, any key to stop");

  DDRC = 0xff;
  DDRD = 0xff;
  DDRF = 0xff;

  while( !UART_char_to_read_check()) {
    PORTC = d;
    PORTD = d;
    PORTF = d;
    if( d == 0x80)
      d = 1;
    else
      d <<= 1;
    _delay_ms(1);
  }

  // restore normal pins
  pin_setup();
}


//
// reset the DDS and restore pins to default states
//
void cmd_reset(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  pin_setup();

  dds_reset();
}

//
// set FSK output (PA6) to toggle with specified us delay
// until any key hit
// or set to L or H
//
void cmd_fsk(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  uint8_t a;

  if( *args[0] == 'H')
    set_pin( DDS_FSK_PORT, DDS_FSK, 1);
  else if( *args[0] == 'L')
    set_pin( DDS_FSK_PORT, DDS_FSK, 0);
  else {
    a = strtoul( args[0], NULL, 0);

    UART_print("Running FSK, any key to stop");

    while( !UART_char_to_read_check()) {
      set_pin( DDS_FSK_PORT, DDS_FSK, 1);
      _delay_us( 20);
      set_pin( DDS_FSK_PORT, DDS_FSK, 0);
      _delay_us( 20);
    }
  }
}
