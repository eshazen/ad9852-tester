//
// AD9852 DDS chip tester for ATMega325
//
// Intended for testing DDS chips on AD eval board or equivalent with
// a 40-pin connector (20 signals plus 20 GND), with pins mapped as such:
//   (represented in dds.h)
//
//      MRESET   PWR_GOOD    PD0
//      OSK      TMS         PD1
//      PMODE    TDO         PD2
//      RDB      TX_IPMC     PD3
//      WRB      TX_ZYNQ     PD4
//      I/O UD   SENSE_SDA   PD5
//      A0       PWR_EN      PD6
//      A1       FPGA_GPIO0  PD7
//      A2       FPGA_GPIO1  PC0
//      A3       FPGA_GPIO2  PC1
//      A4       CPLD_GPIO0  PC2
//      A5       CPLD_GPIO1  PC3
//      D0       EN          PC4
//      D1       TCK         PC5
//      D2       TDI         PC6
//      D3       RX_IPMC     PC7
//      D4       RX_ZYNQ     PF7
//      D5       SENSE_SCL   PF6
//      D6       MON_RX      PF5
//      D7       PS_RST      PF4


// to add a command, add to lists marked by "Edit: cmds"
// and then add the code in cmds.h and cmds.c


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <avr/pgmspace.h>
#include "UART_funcs.h"
#include "utils.h"
#include "cmds.h"
#include "SPI_cmds.h"
#include "NeoPixel.h"

#define COMMAND_SIZE 32

#define PROMPT_CHARACTER '>'

// Edit: cmds
#define CMD_COUNT 9

typedef void (*cmd_pointer)(char*[MAX_ARGS],uint8_t[MAX_ARGS]);

void cmd_show_ptrs(char*[MAX_ARGS],uint8_t[MAX_ARGS]);
void cmd_help(char*[MAX_ARGS],uint8_t[MAX_ARGS]);

// only a couple of commands for now

// Edit: cmds

const char CMD_echo_name[] PROGMEM = "echo";
const char CMD_help_name[] PROGMEM = "help";
const char CMD_led_name[] PROGMEM = "led";
const char CMD_version_name[] PROGMEM = "version";
const char CMD_walk_name[] PROGMEM = "walk";
const char CMD_FSK_name[] PROGMEM = "fsk";

const char CMD_read_name[] PROGMEM = "read";
const char CMD_reset_name[] PROGMEM = "reset";
const char CMD_write_name[] PROGMEM = "write";


// Edit: cmds
const char * const cmd_names[CMD_COUNT] PROGMEM = {CMD_echo_name, CMD_help_name, CMD_led_name, \
  CMD_version_name, CMD_read_name, CMD_write_name, CMD_walk_name, CMD_reset_name, CMD_FSK_name};

// Edit: cmds
const cmd_pointer cmds[CMD_COUNT] PROGMEM = {&cmd_echo, &cmd_help, &cmd_led, &cmd_version, &cmd_read, &cmd_write, &cmd_walk, &cmd_reset, &cmd_fsk};

static char command_line[COMMAND_SIZE+MAX_ARGS+1]; //add an additional byte for a null terminator
static uint8_t char_count = 0;
char empty_string[] = "";
char * empty_strings[4] = {empty_string,empty_string,empty_string,empty_string};
static char * positionbuff[MAX_ARGS];
static uint8_t sizebuff[MAX_ARGS];

uint8_t char_to_read = 0;

//------------------------------------------------------------------------------
// void initialize_fpga(void);
void buffer_overflow(void);
void process_char(void);
void command_run(void);
void tokenize(void);
void shift_command_line(int);
uint8_t cmd_cmp(void);

volatile uint8_t pixel_number = 0;
volatile uint8_t red_value_0 = 0;
volatile uint8_t green_value_0 = 0;
volatile uint8_t blue_value_0 = 0;

volatile uint8_t led_switch = 1;
volatile uint16_t seconds_elapsed = 0;

volatile uint8_t led_on = 0;
volatile uint8_t led_off = 0;

int main (void)
{
  UART_setup();
  //  SPI_setup();
  timer_setup();
    
  memset(command_line,0,sizeof(command_line));
  memcpy(positionbuff,empty_string,MAX_ARGS);
  memset(sizebuff,0,MAX_ARGS);
  
  pin_setup();

  uint8_t d;

  d = MCUCR;
  UART_print_hex( d);
  UART_newline();

  // disable JTAG
  MCUCR = 0x80;

  d = MCUCR;
  UART_print_hex( d);
  UART_newline();

  while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR0 = PROMPT_CHARACTER; //send the prompt character

  for (;;) // Loop forever
    {
      if(UART_char_to_read_check()){ //check if a character is waiting to be processed (has been sent but not read)
	process_char();  
      }
//      if( led_on) {
//	PORTE = 0xf0;
//	led_on = 0;
//      }
//      if( led_off) {
//	PORTE = 0;
//	led_off = 0;
//      }
    }
  return 0; 
}


//------------------------------------------------------------------------------
ISR(TIMER1_OVF_vect)
{
  if(led_switch){
    seconds_elapsed++;
  }
  led_switch ^= 1;
  if (led_switch){
    led_on = 1;
    // test only output 55H for testing
    //    UDR0 = 0x55;
  }else {
    led_off = 1;
  }
}

//------------------------------------------------------------------------------
void cmd_help(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //char** cmds_ptr = (char**)pgm_read_word(cmd_names);
  for(uint8_t command_number = 0;command_number < CMD_COUNT; command_number++){  
    char* string = (char*)pgm_read_word(cmd_names+command_number);
    char character = pgm_read_byte(string);
    while(character != '\0')
      {
	while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
	UDR0 = character;
	character = pgm_read_byte(++string);
       }	 
     UART_print("");
  }
  //  print_UART(); //print microseconds elapsed
  //  micro_print_hex(seconds_elapsed); //print seconds elapsed
}

//------------------------------------------------------------------------------
void cmd_show_ptrs(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  char* cmd_address = (char*)&(command_line[0]);
  UART_print_hex((uint16_t)cmd_address);
  UART_print_noline("   ");

  for(int iChar = 0;iChar<char_count;iChar++){
    while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
    if(command_line[iChar] != 0x00){
      UDR0 = command_line[iChar];
    }
    else{
      UDR0 = '~'; //print ~
    }
    //(I think this is redundant) while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  }

  UART_print("");

  for(uint8_t argnum = 0; argnum<MAX_ARGS; argnum++){
    if(arg_size[argnum] > 0){
      UART_print_hex(argnum);
      UART_print_noline("   ");
      UART_print_hex(sizebuff[argnum]);
      UART_print_noline("   ");
      char* pos_address = (char*)(positionbuff[argnum]);
      UART_print_hex((uint16_t)pos_address);
      UART_print_noline("   ");
      UART_print(positionbuff[argnum]);
    }
  }
}

//------------------------------------------------------------------------------
void buffer_overflow(void) 
{
  UART_print("    buffer overflow");  
  memset(command_line,0,sizeof(command_line));
  char_count = 0;
  UART_newline();
  while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR0 = PROMPT_CHARACTER; // send '>' character
  while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR0 = '\a';//send the bell character
}

//------------------------------------------------------------------------------
void command_run(void)
{ 
  tokenize();
  // command_line[char_count] = 0x0; 
  UART_newline();
  uint8_t cmd_num = 0;
  cmd_num = cmd_cmp();
  if((cmd_num < 0) || (cmd_num >= sizeof(cmds))){
    UART_print("Bad Command");
  }
  else {
    cmd_pointer entered_command = (cmd_pointer)pgm_read_word(cmds + cmd_num);
    entered_command(positionbuff,sizebuff);
  }
  UART_newline();
  char_count = 0;
  while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it
  UDR0 = PROMPT_CHARACTER; // send '>' character
  memset(command_line,0,sizeof(command_line));
}

//------------------------------------------------------------------------------
inline void process_char(void)
{
  command_line[char_count] = UART_increment_interrupt_read();
  if(command_line[char_count] == 0x8){ //if backspace has been entered
    if(char_count > 0){
      command_line[char_count] = 0x0;
      backspace_func();
      char_count--;
    }
  }else if (command_line[char_count] == 0xA || command_line[char_count] == 0xD){ //if newline or carriage return is entered
    command_run();
  }else if(char_count == 32){ //if too long a command has been entered
    buffer_overflow();
  }else if (command_line[char_count] < 0x80 && command_line[char_count] >= 0x20){
    while (( UCSR0A & (1 << UDRE0 )) == 0) {}; // Do nothing until UDR is ready for more data to be written to it 
    UDR0 = command_line[char_count]; // Echo back the received byte back to the computer
    char_count++;
  }else{ //if an invalid character is entered
    command_line[char_count] = 0x0; //change invalid character to a null character
    // UART_print("invalid character");
  }
}
//------------------------------------------------------------------------------
void tokenize(void)
{
  memcpy(positionbuff,empty_string,MAX_ARGS); //clear positionbuff
  memset(sizebuff,0,MAX_ARGS); //clear sizebuff
  uint8_t temp_sizebuff[MAX_ARGS+2]; //5 large to also store the command
  memset(temp_sizebuff,0,MAX_ARGS+2); //clear temp_sizebuff
  char * temp_positionbuff[MAX_ARGS+1];
  memcpy(temp_positionbuff,empty_string,MAX_ARGS+1); //clear positionbuff
  int argument_value = 0;
  
  uint8_t command_line_position = 0;
  char current_char;
  
  if (command_line[char_count-1] != ' '){  //if a space  wasn't entered last
    command_line[char_count] = ' '; // add a space  to the end
    char_count++;
  } 

  //command loop
  for (; command_line_position<char_count+1; command_line_position++){
    current_char = command_line[command_line_position] = to_lower(command_line[command_line_position]); //change all uppercase characters to lowercase
    if (current_char  == 0x00){ //if a null terminator is entered before any space
      return;
    }else if (current_char  == ' '){ //tokenizing will begin if a space is entered
      command_line[command_line_position]= 0x00; //change the space to a null terminator
      command_line_position++; //move on to the next character before breaking the loop
      break;
    }
  }
  //argument loop
  uint8_t parsing_arg = 0; //0 if waiting for an argument, 1 if currently parsing argument
  for (; command_line_position<char_count+1; command_line_position++){
    current_char = command_line[command_line_position] = to_lower(command_line[command_line_position]); //change all uppercase characters to lowercase
    if (parsing_arg == 0){ //if waiting to find the first char of an argument
      if (current_char == 0x00){ //if a null terminator is entered before any space
	return;
      }else if (current_char == ' '){ //tokenizing will begin if a space is entered
        command_line[command_line_position] = 0x00; //change the space to a null terminator
      }else {//if a valid character is entered
	positionbuff[argument_value] = command_line+command_line_position; //set positionbuff
	parsing_arg = 1;
	sizebuff[argument_value]++;
      }
    }else { //if currently parsing an argument
      if (current_char == 0x00){ //if a null terminator is entered before any space
	return;
      }else if (current_char == ' '){ //tokenizing will begin if a space is entered
        command_line[command_line_position] = 0x00; //change the space to a null terminator
       	argument_value++;
	parsing_arg = 0;
	if (argument_value > MAX_ARGS){ //there cannot be more than MAX_ARGS arguments
	  UART_print ("too many arguments");
	  return;
	}
      }else if (sizebuff[argument_value] == 8){ //if the current arg is too long, split it up
	shift_command_line(command_line_position);
	command_line_position++;
	argument_value++; //start a new argument
	sizebuff[argument_value]++; //set sizebuff
	positionbuff[argument_value] = command_line+command_line_position; //set positionbuff
      }else { //if the character is valid
	sizebuff[argument_value]++; //set sizebuff
      }
    } 
  }
}
//------------------------------------------------------------------------------
void shift_command_line(int command_line_position){
  //UART_print(" 8 long");
  for(int char_shift = char_count+1; char_shift>command_line_position; char_shift--){ 
    command_line[char_shift] = command_line[char_shift-1];
  }
  command_line[command_line_position] = 0x0; // null terminate the string
  char_count++; //increase the length of the string entered to command line by 1
}

//------------------------------------------------------------------------------
uint8_t cmd_cmp(void)
{
  for(uint8_t command_number = 0;command_number < CMD_COUNT; command_number++){  
    if(strcmp_P(command_line,(char*)pgm_read_word(cmd_names+command_number)) == 0){ //if the strings are the same
      return command_number; //return which command was matched
    }
  }
  return -1;
}
