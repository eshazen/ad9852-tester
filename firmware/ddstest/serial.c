
#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
  DDRE = 0xFF; //Makes PORTE as Output
  int count = 0b00000001;
  while(1) //infinite loop
  {
    PORTE = count << 4; // assigns values to the LEDs
    _delay_ms(100); //1 second delay
    count += 0b00000001; // increment by 1
  }
}
