
#include "dds.h"
#include "utils.h"

#include <util/delay.h>

void dds_setup() {
  // set pins out except data
  DDRD = 0xff - DDS_IO_UD;	/* port D - all output except IO_UD_CLK */
  DDRC = 0xf;			/* port C - low 4 bits out (A2-A5) */
  DDRF = 0;			/* port F - all inputs */
  DDRA = 0xff;			/* port A - all outputs for now */

  DDRE = 0xFF; //Makes PORT E as Output for LEDs
  dds_leds( 0xa);

  // set pins to reasonable defaults - first control signals
  set_pin( DDS_MRESET_PORT, DDS_MRESET, 0); /* reset low */
  set_pin( DDS_OSK_PORT ,DDS_OSK, 0);	    /* OSK low */
  set_pin( DDS_PMODE_PORT, DDS_PMODE, 1); /* PMODE high (parallel mode) */

  set_pin( DDS_WRB_PORT, DDS_WRB, 1);	  /* /WR high */
  set_pin( DDS_RDB_PORT, DDS_RDB, 1);	  /* /RD high */

  set_pin( DDS_FSK_PORT, DDS_FSK, 0); /* FSK low */
}

// strobe reset
void dds_reset() {
  set_pin( DDS_MRESET_PORT, DDS_MRESET, 0); /* reset low */
  set_pin( DDS_MRESET_PORT, DDS_MRESET, 1); /* reset high */
  _delay_us( 50);
  set_pin( DDS_MRESET_PORT, DDS_MRESET, 0); /* reset low */
}

// set LEDs
void dds_leds( uint8_t n) {
  DDS_LED0_PORT &= 0xf;
  DDS_LED0_PORT |= (n << 4);
  set_pin( DDS_LED0_PORT, DDS_LED0, n & (1<<0));
  set_pin( DDS_LED1_PORT, DDS_LED1, n & (1<<1));
  set_pin( DDS_LED2_PORT, DDS_LED2, n & (1<<2));
  set_pin( DDS_LED3_PORT, DDS_LED3, n & (1<<3));
}

// assert address on A0-A5
void dds_addr( uint8_t a) {
  set_pin( DDS_A0_PORT, DDS_A0, a & (1<<0));
  set_pin( DDS_A1_PORT, DDS_A1, a & (1<<1));
  set_pin( DDS_A2_PORT, DDS_A2, a & (1<<2));
  set_pin( DDS_A3_PORT, DDS_A3, a & (1<<3));
  set_pin( DDS_A4_PORT, DDS_A4, a & (1<<4));
  set_pin( DDS_A5_PORT, DDS_A5, a & (1<<5));
}

// assert data on D0-D7
void dds_data( uint8_t d) {
  set_pin( DDS_D0_PORT, DDS_D0, d & (1<<0));
  set_pin( DDS_D1_PORT, DDS_D1, d & (1<<1));
  set_pin( DDS_D2_PORT, DDS_D2, d & (1<<2));
  set_pin( DDS_D3_PORT, DDS_D3, d & (1<<3));
  set_pin( DDS_D4_PORT, DDS_D4, d & (1<<4));
  set_pin( DDS_D5_PORT, DDS_D5, d & (1<<5));
  set_pin( DDS_D6_PORT, DDS_D6, d & (1<<6));
  set_pin( DDS_D7_PORT, DDS_D7, d & (1<<7));
}


// set D0-D7 to be outputs
void dds_data_out() {
  DDRC = 0xff;			/* upper 4 bits out (D0-D3) */
  DDRF = 0xf0;
}

// set D0-D7 to be inputs
void dds_data_in() {
  DDRC = 0xf;			/* port C - low 4 bits out only */
  DDRF = 0;			/* port F - all inputs */
}


void dds_write( uint8_t a, uint8_t d) {
  dds_addr( a);
  dds_data( d);
  dds_data_out();			      /* set data pins to output mode */
  set_pin( DDS_WRB_PORT, DDS_WRB, 0); /* /WR = low */
  set_pin( DDS_WRB_PORT, DDS_WRB, 1); /* /WR = high */
  dds_data_in();			      /* set data pins back to input mode */
}


uint8_t dds_read( uint8_t a) {
  uint8_t d;
  dds_addr( a);
  dds_data_in();
  set_pin( DDS_RDB_PORT, DDS_RDB, 0); /* /RD = 0 */
  d = get_dds_data();
  set_pin( DDS_RDB_PORT, DDS_RDB, 1); /* /RD = 1 */
  return d;
}


uint8_t get_dds_data() {
  uint8_t d = 0;
  if( get_pin( DDS_D0_PIN, DDS_D0)) d |= (1<<0);
  if( get_pin( DDS_D1_PIN, DDS_D1)) d |= (1<<1);
  if( get_pin( DDS_D2_PIN, DDS_D2)) d |= (1<<2);
  if( get_pin( DDS_D3_PIN, DDS_D3)) d |= (1<<3);
  if( get_pin( DDS_D4_PIN, DDS_D4)) d |= (1<<4);
  if( get_pin( DDS_D5_PIN, DDS_D5)) d |= (1<<5);
  if( get_pin( DDS_D6_PIN, DDS_D6)) d |= (1<<6);
  if( get_pin( DDS_D7_PIN, DDS_D7)) d |= (1<<7);
  return d;
}
