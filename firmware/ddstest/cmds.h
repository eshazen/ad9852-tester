#ifndef __CMDS_H__
#define __CMDS_H__
#include <stdint.h>
#define MAX_ARGS 4

extern volatile uint8_t fpga_status; //1 if fpgas were programmed, 0 if not

void cmd_echo(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_led(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_version(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_write(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_read(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_walk(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS]);

void cmd_reset(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS]);

void cmd_fsk(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS]);

#endif
