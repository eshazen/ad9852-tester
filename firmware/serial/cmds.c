#include <stdlib.h>
#include <ctype.h>
#include "utils.h"
#include "cmds.h"
#include "SPI_cmds.h"
#include "UART_funcs.h"
#include "NeoPixel.h"

#define TDC_FW_2X 0x171240 //two times the size of the hex file

#define FLASH_BUFF_SIZE 128 //number of bytes to be writen to and read from the flash per read/write 
#define FLASH_TO_FPGA_SIZE 1

volatile uint8_t fpga_status = 0; //initially the fpgas are unprogrammed

const char* version_num = "1.00";

uint8_t flashbuff[FLASH_BUFF_SIZE];
uint32_t flash_address = 0;

uint8_t program_interrupt = 0;

//------------------------------------------------------------------------------
void cmd_echo(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  //Serial.println("ECHO:");
  for (uint8_t position = 0;(position<MAX_ARGS)&&(arg_size[position] !=0); position++){
    UART_print_noline(args[position]);
    // if (args[position][1] != 0x00 || 0x20){
    UART_print_noline(" ");
    //}
  }
  UART_newline();        
}


//------------------------------------------------------------------------------
void cmd_led(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  uint8_t led_value = strtoul( args[0], NULL, 0);
  PORTE = led_value << 4;
}

//------------------------------------------------------------------------------
void cmd_version(char * args[MAX_ARGS], uint8_t arg_size[MAX_ARGS])
{
  UART_print_noline("DDS Tester ");
  UART_print_noline(version_num);
}
