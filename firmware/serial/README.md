# simple serial monitor

This implements a serial monitor using the USB UART to process commands.
For some reason it only works at 1200 baud currently (faster rates have big baud rate errors- need to debug)

As of now the only commands supported are:

* help - display command list
* led - set binary value on 4 LEDs
* echo - echo the command line
* version - display firmware version

