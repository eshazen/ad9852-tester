#ifndef __CMDS_H__
#define __CMDS_H__
#include <stdint.h>
#define MAX_ARGS 4

extern volatile uint8_t fpga_status; //1 if fpgas were programmed, 0 if not

void cmd_echo(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_led(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

void cmd_version(char * [MAX_ARGS], uint8_t [MAX_ARGS]);

#endif
